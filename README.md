**Have an Adventure**

The travel and hospitality field is one area that will be growing as the baby boomers retire.  With spare time on their hands, couples who are retired will want to take time to travel and relax.  One aspect of employment here is to put your mind where theirs is.
 
For example these people will want to travel, but to where?   The dollar is only worth at this moment 80 cents compared to the Euro, the currency used in much of Europe.  Travel to Europe is; therefore, more expensive.  Also, many people now in America aren’t of recent European ancestry.  There is no family pull to visit there for many.
 
 As far as travel to other parts of the world, Latin America is plain scary to many Americans because of things like the drug war in Mexico.  Other places have seen tourism drop to near zero because of turbulence abroad.  Too Asia; except for Japan, is viewed as very foreign to many people over here. And please remember, this is how it is viewed by many Americans that I am writing about.  Chile is very nice and safe; for instance, but how many Americans know this?  Israel is also very safe, beautiful and fascinating, but many people here don’t know this either.
 
 At any rate, many people who are retiring are staying in the good old USA for vacations it turns out.  Also, with a view that the world is getting more dangerous and the weak dollar meaning that good deals can be had in America, many people from around the world are visiting the United States.
 
 So, one place that is attracting vacationers is the U.S. Virgin Islands.  Tourism is making a comeback here, both attracting Americans as it’s American Territory and people from Europe who find that the Euro goes further here in purchasing power than at home.
 
 Now to go about a job hunt in a place like the Virgin Islands, I don’t recommend you just pop in and say can I have a job.  Traveling to a job you have is fun.  Traveling to a place without a job where you don’t know people; especially when young when you don’t have the wherewithal that older people have can be dangerous.

When applying for a job in the hospitality area where you are going to travel to a new location then, use a reputable recruiting company.If you want to see the right example of perfect hospitality,visit [hotel st gallen](http://www.oberwaid.ch/). And I cannot stress reputable enough here.  Check the company on-line, check it with the better business bureau and call the local police in the area you want to work to see if they have heard bad things about the company.
 
 Another idea for job hunting is find out what the local paper’s name is for where you want to work, and see if you can pull up the on-line newspaper.  If you can, you can check the want ads from the source itself.  Again, use commonsense and check out your employer with the local police.
 
 If you are young and going on vacation there with your folks and the hospitality field is a career idea you like, you can ask the right questions when get down there.  Just please don’t stay behind.  Do it right with a plan, not as a latest career fad.  I have seen enough of those in my life, and the people following them don’t last long in any career.
 
 

